| Track 				| GT3 First Choice 	| GT3 Second Choice	| GT4 yay/nay
| ----- 				| ----------------	| -----------------	| -----------
| Autodromo Enzo e Dino Ferrari		| Honda 		| Porsche 		| yay
| Barcelona				| Porsche 		| Honda			| yay
| Brands Hatch				| Porsche		| ---			| yay
| Circuit of the Americas		| ---			| ---			| nay
| Donington Park			| Porsche		| Mercedes		| yay
| Hungaroring				| Porsche		| ---			| yay
| Indianapolis Motor Speedway		| Ferrari		| ---			| yay
| Kyalami				| Porsche		| ---			| yay
| Laguna Seca				| Porsche		| Honda			| yay
| Misano				| Honda			| Ferrari		| yay
| Monza					| Honda 		| Porsche		| nay
| Mount Panorama			| ---			| ---			| yay
| Nürburgring				| Porsche		| Honda			| yay
| Oulton Park				| Porsche		| ---			| yay
| Paul Ricard				| Honda			| Porsche		| nay
| Silverstone				| Porsche		| ---			| yay
| Snetterton Circuit			| Porsche		| Honda / Mercedes	| yay
| Spa-Francorchamps			| Honda			| McLaren		| nay
| Suzuka				| Porsche		| Honda			| yay
| Watkins Glen International		| Honda			| Porsche		| yay
| Zandvoort				| Porsche		| ---			| yay
| Zolder				| ---			| ---			| nay
_____
Counts:
Porsche:	12 | 04   
Honda:		06 | 04.5	   
Ferrari:	01 | 01   
Mercedes:	00 | 01.5   
McLaren:	00 | 01   
 
GT4 yay/nay:	17 | 5
